+++
title = "About KiCad"
date = "2021-05-28"
aliases = [ "/about/" ]
categories = [ "About" ]
summary = "The history of KiCad and its development"
[menu.main]
    parent = "About"
    name   = "KiCad"
+++

{{< aboutlink "/img/about/kicad-logo.png" "/img/kicad_logo_paths.svg" >}}

KiCad is an http://en.wikipedia.org/wiki/Open_source[open source] software suite for
http://en.wikipedia.org/wiki/Electronic_design_automation[Electronic Design Automation] (EDA).
The programs handle http://en.wikipedia.org/wiki/Schematic_capture[Schematic Capture],
and http://en.wikipedia.org/wiki/Printed_circuit_board[PCB] Layout with
http://en.wikipedia.org/wiki/Gerber_format[Gerber] output. The suite runs on Windows, Linux
and macOS and is licensed under http://en.wikipedia.org/wiki/GNU_General_Public_License[GNU GPL v3].

== Mission Statement

The goal of the KiCad project is to provide the best possible cross platform electronics design
application for professional electronics designers.  Every effort is made to hide the complexity
of advanced design features so that KiCad remains approachable by new and inexperienced users,
but when determining the direction of the project and the priority of new features, the needs of
professional users take precedence.

== Project Governance

The KiCad project is governed by a technical committee made up of the members of the lead
development team.  Most decisions are made by a consensus of the technical committee.  When
a consensus cannot be reached the final decision is made by the project lead developer.

<!--more-->
== History
KiCad was first released in 1992 by its original author, Jean-Pierre Charras.
It has been in continual development since then and is now managed by the
KiCad Development Team.

The name of KiCad comes from the first letters of a company of Jean-Pierre Charras' friend "Ki"
being combined with "Cad". But it now has no meaning other than being the name of the software
suite. https://lists.launchpad.net/kicad-developers/msg27528.html[Mentioned by Jean-Pierre in an email.]

More KiCad history, general information, and advancements can be found in Wayne's
https://www.youtube.com/watch?v=wRolB1my6fI[2015],
http://video.fosdem.org/2016/aw1121/kicad-project-status.webm[2016],
https://video.fosdem.org/2017/AW1.120/kicad_status.vp8.webm[2017],
https://video.fosdem.org/2018/K.4.201/cad_kicad_v5.webm[2018],
https://video.fosdem.org/2019/AW1.125/kicad.webm[2019],
https://video.fosdem.org/2020/H.2213/kicad.webm[2020], and
https://video.fosdem.org/2021/D.cad/kicad.webm[2021]
FOSDEM presentations.


== Lead Developers
[role="table table-striped table-condensed"]
|===
|Roberto Fernandez Bautista|
|Jean-Pierre Charras|
|Jon Evans|
|Seth Hillbrand|
|Ian McInerney|
|Thomas Pointhuber|
|Mark Roszko|
|Wayne Stambaugh (Project Leader)|
|Tomasz Wlostowski|
|Jeff Young|
|===

== Developers Emeriti
[role="table table-striped table-condensed"]
|===
|John Beard
|Dick Hollenbeck
|Alexis Lockwood
|Brian Sidebotham
|Orson (Maciej) Sumiński
|===

